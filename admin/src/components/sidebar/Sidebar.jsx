import "./sidebar.css";
import { useDispatch } from "react-redux"
import { logout } from "../../redux/userSlice";
import { useState, useEffect } from "react"
import {
  BarChart,
  Timeline,
  TrendingUp,
  PermIdentity,
  AttachMoney,
  KingBedOutlined,
  BeachAccess,
  ExitToApp
} from "@material-ui/icons";
import { Link } from "react-router-dom";

export default function Sidebar() {

  const dispatch = useDispatch()
  const logoutH = async () => {
    try {
      localStorage.clear()
      document.location.reload()
      dispatch(logout())
      window.alert("Вихід здійснено успішно")
    } catch (error) {
      console.log(error)
      window.alert("Вихід здійснено невдало")
    }
  }

  return (
    <div className="sidebar">
      <div className="sidebarWrapper">
        <div className="sidebarMenu">
          <h3 className="sidebarTitle">Інформаційна панель</h3>
          <ul className="sidebarList">
            <Link to="/" className="link">
              <li className="sidebarListItem active">
                <BarChart className="sidebarIcon" />
                Статистика
              </li>
            </Link>
          </ul>
        </div>
        <div className="sidebarMenu">
          <h3 className="sidebarTitle">Взаємодія</h3>
          <ul className="sidebarList">
            <Link to="/users" className="link">
              <li className="sidebarListItem">
                <PermIdentity className="sidebarIcon" />
                Користувачі
              </li>
            </Link>
            <Link to="/rooms" className="link">
              <li className="sidebarListItem">
                <KingBedOutlined className="sidebarIcon" />
                Кімнати
              </li>
            </Link>
            <Link to="/entertainments" className="link">
              <li className="sidebarListItem">
                <BeachAccess className="sidebarIcon" />
                Розваги
              </li>
            </Link>
            <Link to="/orders" className="link">
              <li className="sidebarListItem">
                <AttachMoney className="sidebarIcon" />
                Замовлення
              </li>
            </Link>

            <li className="sidebarListItem" onClick={logoutH}>
              <ExitToApp className="sidebarIcon" />
              Вихід
            </li>

          </ul>
        </div>
      </div>
    </div>
  );
}
