import "./featuredInfo.css";
import { ArrowDownward, ArrowUpward } from "@material-ui/icons";

export default function FeaturedInfo({ sales, count, user }) {
  return (
    <div className="featured">
      <div className="featuredItem">
        <span className="featuredTitle">Прибуток</span>
        <div className="featuredMoneyContainer">
          <span className="featuredMoney">$ {sales.total}</span>
          <span className="featuredMoneyRate">
          </span>
        </div>
        <span className="featuredSub">Прибуток за місяць</span>
      </div>
      <div className="featuredItem">
        <span className="featuredTitle">Кількість</span>
        <div className="featuredMoneyContainer">
          <span className="featuredMoney">{count.count}</span>
          <span className="featuredMoneyRate">
          кімнат/розваг
          </span>
        </div>
        <span className="featuredSub">Бронювань за місяць</span>
      </div>
      <div className="featuredItem">
        <span className="featuredTitle">Користувачі</span>
        <div className="featuredMoneyContainer">
          <span className="featuredMoney">{user.users}</span>
          <span className="featuredMoneyRate">
            реєстрацій
          </span>
        </div>
        <span className="featuredSub">Зареєструвань за рік</span>
      </div>
    </div>
  );
}
