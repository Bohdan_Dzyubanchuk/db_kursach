import React from "react";
import "./topbar.css";
import { NotificationsNone, Language, Settings } from "@material-ui/icons";
import MaBoMaIMG from '../../source/MaBoMa.png'

export default function Topbar() {
  return (
    <div className="topbar">
      <div className="topbarWrapper">
        <div className="topLeft">
          <div>
            <img src={MaBoMaIMG} alt="MaBoMa" width={100}></img>
          </div>
        </div>
        <div className="topRight">
          <span className="logo">Панель адміністратора</span>
        </div>
      </div>
    </div>
  );
}
