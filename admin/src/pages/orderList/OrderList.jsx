import "./userList.css";
import { DataGrid } from "@material-ui/data-grid";
import { DeleteOutline, MonetizationOnSharp } from "@material-ui/icons";
import { userRows } from "../../dummyData";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import { userRequset } from "../../requestMethod";

export default function OrderList() {
  const [data, setData] = useState(userRows);
  const [service, setService] = useState(null);

  useEffect(
    () => {
      const getProducts = async () => {
        try {
          const res = await userRequset.get('http://localhost:5000/api/service')
          setService(res.data);
        } catch (e) {
          console.log(e)
        }
      }
      getProducts()
    }, []
  )
  const [user, setUser] = useState(null);
  //console.log(JSON.parse(JSON.parse(localStorage.getItem("persist:root")).currentUser).accessToken)
  useEffect(() => {
    const getProduct = async () => {
      try {
        const res = await userRequset.get("/user/");
        const usersFromDB = res.data;
        usersFromDB.forEach(element => element.id = element._id);
        setUser(res.data)
      } catch (e) {
        console.log(e)
      }
    }
    getProduct();
  }, [])
  useEffect(
    () => {
      const getProducts = async () => {
        try {
          const res = await userRequset.get('http://localhost:5000/api/orders')
          const usersFromDB = res.data
          usersFromDB.forEach(element => {
            element.id = element._id;
            let phoneNumber = "";
            let username = "";
            let serviceName = "";
            user.forEach(el => {

              if (el._id == element.userId) {
                phoneNumber = el.phone;
                username = el.username;
              }
            })
            service.forEach(el => {
              if (el._id == element.service.serviceId) {
                serviceName = el.title;
              }
            })
            element.serviceName = serviceName
            element.phone = phoneNumber
            element.username = username
          });
          setData(res.data);
        } catch (e) {
          console.log(e)
        }
      }
      getProducts()
    }, [user]
  )
  const handleDelete = async (id) => {
    const res = window.confirm("Ви справді хочете видалити даний запит?")
    if (res) {
      try {
        const response = await userRequset.delete(`orders/${id}`);
        alert("Видалено успішно")
      } catch (e) {
        console.log(e)
      }
    }
  };
  const handleAgree = async (id) => {
    const res = window.confirm("Ви справді хочете погодити даний запит?")
    const serviceID = data.filter(e => e._id === id)[0].service.serviceId
    console.log(serviceID)
    if (res) {
      try {
        const responseSe = await userRequset.put(`service/${serviceID}`, { isBooked: true });
        const responseOr = await userRequset.put(`orders/${id}`, { status: "agreed" });
        alert("Оновлено успішно")
      } catch (e) {
        console.log(e)
      }
    }
  };
  const detalis = (id) => {
    const detailsOrder = data.filter((item) => item.id === id)[0]
    window.alert(
      `
    Назва елементу: ${detailsOrder.serviceName}\n
    Бронювання за номером: ${detailsOrder.phone}\n
    Дата початку терміну бронювання: ${detailsOrder.service.dateFrom}\n
    Дата кінця терміну бронювання: ${detailsOrder.service.dateTo}\n
    Загальна сума: $ ${detailsOrder.amount} 
      `
    )
  }
  const columns = [
    { field: "id", headerName: "ID", width: 90 },
    {
      field: "username",
      headerName: "Логін",
      width: 200,
      renderCell: (params) => {
        return (
          <div className="userListUser">
            {params.row.username}
          </div>
        );
      },
    },
    { field: "phone", headerName: "Номер телефону", width: 200 },
    {
      field: "status",
      headerName: "Статус",
      width: 160,
    },
    {
      field: "amount",
      headerName: "Cума",
      width: 160,
    },
    {
      field: "action",
      headerName: "Дії",
      width: 150,
      renderCell: (params) => {
        return (
          <>

            <button className="userListEdit" onClick={() => detalis(params.row.id)}>Детальніше</button>

            |
            <MonetizationOnSharp
              className="userListAgree"
              onClick={() => handleAgree(params.row.id)}
            />
            |
            <DeleteOutline
              className="userListDelete"
              onClick={() => handleDelete(params.row.id)}
            />
          </>
        );
      },
    },
  ];

  return (
    <div className="userList">
      <DataGrid
        rows={data}
        disableSelectionOnClick
        columns={columns}
        pageSize={8}
      />
    </div>
  );
}
