import "./newProduct.css";
import { useState } from "react"
import { userRequset } from "../../requestMethod";

export default function NewService() {

  const [title, setTitle] = useState('');
  const [desc, setDesc] = useState('');
  const [img, setImg] = useState('');
  const [price, setPrice] = useState(1);
  const [category, setCategory] = useState('');
  const [size, setSize] = useState('');
  const [color, setColor] = useState('');

  const createService = async (e) => {
    e.preventDefault()
    const newService = {
      title,
      desc,
      img,
      size,
      color,
      price,
      categories: category
    }
    try {
      const addedService = await userRequset.post('/service/', newService)
      alert("Додано успішно")
    } catch (error) {
      alert(error.message)
    }
  };

  console.log(category);
  return (

    <div className="newProduct">
      <h1 className="addProductTitle">Новий сервіс</h1>
      <form className="addProductForm">
        <div className="addProductItem">
          <label>Зображення</label>
          <input type="text" placeholder="Apple Airpods" name="img" onChange={e => setImg(e.target.value)} />
          <img src={img} class="productInoImg" alt="не існуюча силка"></img>
        </div>
        <div className="addProductItem">
          <label>Назва</label>
          <input type="text" value={title} placeholder="Apple Airpods" name="title" onChange={e => setTitle(e.target.value)} />
        </div>
        <div className="addProductItem">
          <label>Опис</label>
          <textarea name="desc" value={desc} onChange={e => setDesc(e.target.value)}></textarea>
        </div>
        <div className="addProductItem">
          <label>Ціна</label>
          <input type="number" name="price" value={price} placeholder="123" onChange={e => setPrice(e.target.value)} />
        </div>
        <div className="addProductItem">
          <label>Категорія</label>
          <select name="category" id="category" onChange={e => setCategory(e.target.value)}>
            <option disabled selected >Категорія</option>
            <option value="room">Кімната</option>
            <option value="entertainment">Розвага</option>
          </select>
        </div>
        <div className="addProductItem">
          <label>Розмір</label>
          <select name="category" id="category" onChange={e => setSize(e.target.value)}>
            <option disabled selected >Розмір</option>
            <option value="2x">2х</option>
            <option value="3x">3х</option>
          </select>
        </div>
        <div className="addProductItem">
          <label>Колір</label>
          <select name="category" id="category" onChange={e => setColor(e.target.value)}>
            <option disabled selected >Колір</option>
            <option value="black">Чорний</option>
            <option value="white">Білий</option>
          </select>
        </div>
        <button className="addProductButton" onClick={createService} >Додати</button>
      </form>
    </div>
  );
}
