import "./productList.css";
import { DataGrid } from "@material-ui/data-grid";
import { DeleteOutline } from "@material-ui/icons";
import { productRows } from "../../dummyData";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import { userRequset } from "../../requestMethod";

export default function RoomList() {
  const [data, setData] = useState(productRows);


  useEffect(
    () => {
      const getProducts = async () => {
        try {
          const res = await userRequset.get('http://localhost:5000/api/service?category=room')
          const usersFromDB = res.data
          usersFromDB.forEach(element => element.id = element._id);
          setData(res.data);
        } catch (e) {
          console.log(e)
        }
      }
      getProducts()
    }, []
  )
  const handleDelete = async(id) => {
    const res = window.confirm("Ви справді хочете видалити дану кімнату?")
    if (res) {
      try {
        const response = await userRequset.delete(`service/${id}`);
        console.log(response)
      } catch (e) {
        console.log(e)
      }
    }
  };

  const columns = [
    { field: "id", headerName: "ID", width: 90 },
    {
      field: "product",
      headerName: "Кімната",
      width: 200,
      renderCell: (params) => {
        return (
          <div className="productListItem">
            <img className="productListImg" src={params.row.img} alt="" />
            {params.row.title}
          </div>
        );
      },
    },

    {
      field: "isBooked",
      headerName: "Бронювання",
      width: 200,
    },
    {
      field: "price",
      headerName: "Ціна $/доб",
      width: 200,
    },
    {
      field: "action",
      headerName: "Дії",
      width: 200,
      renderCell: (params) => {
        return (
          <>
            <Link to={"/product/" + params.row.id}>
              <button className="productListEdit">Редагувати</button>
            </Link>
            <DeleteOutline
              className="productListDelete"
              onClick={() => handleDelete(params.row.id)}
            />
          </>
        );
      },
    },
  ];

  return (
    <div className="productList">
      <DataGrid
        rows={data}
        disableSelectionOnClick
        columns={columns}
        pageSize={8}
      />
    </div>
  );
}
