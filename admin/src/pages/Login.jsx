import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import styled from "styled-components"
import { login } from "../redux/apiCalls"

const Container = styled.div`

width:100vw;
height:100vh;
background: linear-gradient(
    rgba(0,0,0,0.7),
    rgba(0,0,0,0.2)
    ),
    url("https://images.pexels.com/photos/1171719/pexels-photo-1171719.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1") center;
display:flex;
align-items:center;
justify-content:center;
`
const Wrapper = styled.div`
width:40%;
padding:20px;
border-radius: 10px;
background-color:white;
box-shadow: inset 0em 2em 2em black;
`
const Title = styled.h1`
font-size:24px;
text-align:center;

`
const Form = styled.form`
display:flex;
flex-direction:column;
`
const Input = styled.input`
flex:1;
min-width:40%;
margin:10px 20px;
padding:5px 10px;
font-size: 14px;
`
const Button = styled.button`
font-weight:bold;
font-size: 14px;
background-color: black;
color: white;
border:none;
margin:10px 20px;
padding:10px;
border-radius:5px;
cursor:pointer;
&:hover{
    color:black;
    background-color: #f9a825;
&:disabled{
    background-color: grey;
    cursor:not-allowed;
}
}
`

const LINK = styled.a`
font-weight:600;
 margin:5px;
 font-size:12px;
 text-align:center;
 color:black;
 text-decoration: underline;
  &:hover{
    color:#f9a825;
 }
`
const Error = styled.span`
color:red;
text-align:center;
`
const Login = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const dispatch = useDispatch()
    const { isFetching, error } = useSelector(state => state.user)
    const handleLogin = (e) => {
        e.preventDefault();
        login(dispatch, { username, password })
    }

    return (
        <Container>
            <Wrapper>
                <Title>SING IN</Title>
                <Form>
                    <Input placeholder="username" onChange={e => setUsername(e.target.value)}></Input>
                    <Input placeholder="password" type="password" onChange={e => setPassword(e.target.value.toLowerCase())}></Input>
                    <Button onClick={handleLogin} disabled={isFetching}>LOGIN</Button>
                    {error && <Error>!Something went wrong!</Error>}
                </Form>
            </Wrapper>
        </Container>
    )
}

export default Login