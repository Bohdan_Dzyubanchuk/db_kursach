import Chart from "../../components/chart/Chart";
import FeaturedInfo from "../../components/featuredInfo/FeaturedInfo";
import "./home.css";
import { month } from "../../dummyData";
import WidgetSm from "../../components/widgetSm/WidgetSm";
import WidgetLg from "../../components/widgetLg/WidgetLg";
import { useState, useEffect } from "react"
import { userRequset } from "../../requestMethod";

export default function Home() {
  const [sales, setSales] = useState(null)
  const [count, setCount] = useState(null)
  const [user, setUser] = useState(null)
  const [usersALL, setUsersALL] = useState(null)
  const [orders, setOrders] = useState(null)
  useEffect(() => {
    const getService = async () => {
      try {
        const service = await userRequset.get('orders/incomebymonth')
        setSales(service.data)
        console.log(service.data)
      } catch (error) {
        console.log(error)
      }
    }
    getService();
  }, []);

  useEffect(() => {
    const getService = async () => {
      try {
        const service = await userRequset.get('orders/incomecount')
        setCount(service.data)
        console.log(service.data)
      } catch (error) {
        console.log(error)
      }
    }
    getService();
  }, []);
  useEffect(() => {
    const getService = async () => {
      try {
        const service = await userRequset.get('user/statsbymonth')
        setUser(service.data)
        console.log(service.data)
      } catch (error) {
        console.log(error)
      }
    }
    getService();
  }, []);
  useEffect(() => {
    const getService = async () => {
      try {
        const service = await userRequset.get('user/stats')
        const newUsers = service.data.map(e => ({
          "id": e._id,
          "name": month[+e._id - 1],
          "Registr by month": e.total
        }))
        newUsers.sort((a, b) => a.id - b.id)
        setUsersALL(newUsers)
        console.log(newUsers)
      } catch (error) {
        console.log(error)
      }
    }
    getService();
  }, []);
  useEffect(() => {
    const getService = async () => {
      try {
        const service = await userRequset.get('orders/stats')
        const newOrders = service.data.map(e => ({
          "id": e._id,
          "name": month[+e._id - 1],
          "Registr by month": e.total
        }))
        newOrders.sort((a, b) => a.id - b.id)
        setOrders(newOrders)
        console.log(service)
      } catch (error) {
        console.log(error)
      }
    }
    getService();
  }, []);


  return (
    <div className="home">
      <FeaturedInfo sales={sales ? sales[0] : { total: 0 }} count={count ? count[0] : { count: 0 }} user={user ? user[0] : { users: 0 }} />
      <Chart data={usersALL} title="Графік реєстувань" grid dataKey="Registr by month" />
      <Chart data={orders} title="Графік запитів на бронювання" grid dataKey="Registr by month" />
      {/* <Chart data={usersALL} title="Графік реєстувань" grid dataKey="Registr by month" /> */}
      <div className="homeWidgets">
        {/* <WidgetSm />
        <WidgetLg /> */}
      </div>
    </div>
  );
}
