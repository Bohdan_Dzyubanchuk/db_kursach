import "./userList.css";
import { DataGrid } from "@material-ui/data-grid";
import { DeleteOutline } from "@material-ui/icons";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import { userRequset } from "../../requestMethod";

export default function UserList() {
  const [data, setData] = useState(null);
  //console.log(JSON.parse(JSON.parse(localStorage.getItem("persist:root")).currentUser).accessToken)
  useEffect(() => {
    const getProduct = async () => {
      try {
        const res = await userRequset.get("/user/");
        const usersFromDB = res.data;
        usersFromDB.forEach(element => element.id = element._id);
        setData(res.data)
      } catch (e) {
        console.log(e)
      }
    }
    getProduct();
  }, [])

  const handleDelete = async (id) => {
    const res = window.confirm("Ви справді хочете видалити користувача?")
    if (res) {
      try {
        const response = await userRequset.delete(`user/${id}`);
        console.log(response)
      } catch (e) {
        console.log(e)
      }
    }
  };
  const handleGiveAnAccess = async (id, access) => {
    const res = window.confirm("Ви справді хочете змінити рівень доступу користувача?")
    if (res) {
      try {
        const response = await userRequset.put(`user/${id}`, { isAdmin: !access });
        console.log(response)
      } catch (e) {
        console.log(e)
      }
    }
  };

  const columns = data ? [
    { field: "_id", headerName: "ID", width: 90 },
    {
      field: "username",
      headerName: "Логін",
      width: 200,
      renderCell: (params) => {
        return (
          <div className="userListUser">
            {params.row.username}
          </div>
        );
      },
    },
    { field: "phone", headerName: "Номер телефону", width: 200 },
    {
      field: "isAdmin",
      headerName: "Адмін доступ",
      width: 160,
    },
    // {
    //   field: "transaction",
    //   headerName: "Transaction Volume",
    //   width: 160,
    // },
    {
      field: "action",
      headerName: "Дії",
      width: 150,
      renderCell: (params) => {
        return (
          <>

            <button className="userListEdit" onClick={() => handleGiveAnAccess(params.row.id, params.row.isAdmin)}>Змінити доступ</button>

            <DeleteOutline
              className="userListDelete"
              onClick={() => handleDelete(params.row.id)}
            />
          </>
        );
      },
    },
  ] : null;

  return (
    data ? <div className="userList">
      <DataGrid
        rows={data}
        disableSelectionOnClick
        columns={columns}
        pageSize={8}
      />
    </div> : <></>
  );
}
