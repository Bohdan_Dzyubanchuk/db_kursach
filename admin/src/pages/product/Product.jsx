import { Link, useLocation } from "react-router-dom";
import "./product.css";
import Chart from "../../components/chart/Chart"
import { Publish } from "@material-ui/icons";
import { useEffect, useState } from "react"
import { userRequset } from "../../requestMethod";

export default function Product() {
    const location = useLocation();
    const id = location.pathname.split("/")[2];
    const [data, setData] = useState({});
    useEffect(() => {
        const getService = async () => {
            try {
                const service = await userRequset.get('service/find/' + id)
                setData(service.data)
                setTitle(service.data.title)
                setDesc(service.data.desc)
                setImg(service.data.img)
                setPrice(service.data.price)
                setBooked(service.data.booked)
                setSize(service.data.size)
                setColor(service.data.color)
            } catch (error) {
                console.log(error)
            }
        }
        getService();
    }, []);

    const [title, setTitle] = useState('');
    const [desc, setDesc] = useState('');
    const [img, setImg] = useState('');
    const [price, setPrice] = useState(1);
    const [booked, setBooked] = useState('');
    const [size, setSize] = useState('');
    const [color, setColor] = useState('');

    const createService = async (e) => {
        e.preventDefault()
        const newService = {
            title,
            desc,
            img,
            size,
            color,
            price,
            isBooked: booked
        }
        try {
            const addedService = await userRequset.put('/service/' + id, newService)
            alert("Обновлено успішно")
        } catch (error) {
            alert(error.message)
        }
    };

    console.log(data.desc)
    return (
        <div className="product">
            <div className="productTitleContainer">
                <h1 className="productTitle">Сервіс</h1>
                <Link to="/newservice">
                    <button className="productAddButton">Створити новий</button>
                </Link>
            </div>
            <div className="productTop">
                <div className="productTopLeft">
                    {/* <Chart data={productData} dataKey="Sales" title="Sales Performance" /> */}
                </div>
                <div className="productTopRight">
                    <div className="productInfoTop">
                        <img src={data.img} alt="" className="productInfoImg" />
                        <span className="productName">{data.title}</span>
                    </div>
                    <div className="productInfoBottom">
                        <div className="productInfoItem">
                            <span className="productInfoKey">id:</span>
                            <span className="productInfoValue">{data._id}</span>
                        </div>
                        <div className="productInfoItem">
                            <span className="productInfoKey">sales:</span>
                            <span className="productInfoValue">5123</span>
                        </div>
                        <div className="productInfoItem">
                            <span className="productInfoKey">active:</span>
                            <span className="productInfoValue">yes</span>
                        </div>
                        <div className="productInfoItem">
                            <span className="productInfoKey">in stock:</span>
                            <span className="productInfoValue">no</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="productBottom">
                <form className="productForm">
                    <div className="productFormLeft">
                        <label>Назва</label>
                        <input type="text" placeholder="Назва" value={title} onChange={e => setTitle(e.target.value)} />
                        <label>Опис</label>
                        <textarea value={desc} onChange={e => setDesc(e.target.value)} ></textarea>
                        <label>Зображення</label>
                        <input type="text" placeholder="Зображення" value={img} onChange={e => setImg(e.target.value)} />
                        <label>Ціна</label>
                        <input type="number" placeholder="Ціна" value={price} onChange={e => setPrice(e.target.value)} />
                        <label>Розмір</label>
                        <select name="inStock" id="idStock" onChange={e => setSize(e.target.value)}>
                            <option value="2x">2x</option>
                            <option value="3x">3x</option>
                        </select>
                        <label>Колір приміщення</label>
                        <select name="active" id="active" onChange={e => setColor(e.target.value)}>
                            <option value="black">Чорний</option>
                            <option value="white">Білий</option>
                        </select>
                        <label>Бронювання</label>
                        <select name="active" id="active" onChange={e => setBooked(e.target.value)}>
                            <option value={true}>Заброньовано</option>
                            <option value={false}>Не броньовано</option>
                        </select>
                    </div>
                    <div className="productFormRight">
                        <div className="productUpload">
                            <img src={data.img} alt="Не існуюча силка" className="productUploadImg" />
                            <label for="file">
                            </label>
                            <input type="file" id="file" style={{ display: "none" }} />
                        </div>
                        <button className="productButton" onClick={createService}>Оновити</button>
                    </div>
                </form>
            </div>
        </div>
    );
}
