import Sidebar from "./components/sidebar/Sidebar";
import Topbar from "./components/topbar/Topbar";
import "./App.css";
import Home from "./pages/home/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import UserList from "./pages/userList/UserList";
import User from "./pages/user/User";
import { useState, useEffect } from "react"

import RoomList from "./pages/RoomList/RoomList";
import OrderList from "./pages/orderList/OrderList";
import EntertainmentList from "./pages/EntertainmentList/EntertainmentList";

import Product from "./pages/product/Product";
import NewService from "./pages/newService/newService";
import Login from "./pages/Login";

function App() {
  const [admin, setAdmin] = useState(false)
  const getAdmin = () => {
    try {
      setAdmin(JSON.parse(JSON.parse(localStorage.getItem("persist:root")).currentUser).isAdmin)
    } catch (e) {
      setAdmin(false)
    }
  }
  useEffect(()=>getAdmin(),[])
  
  // useEffect(() => {
  //   document.location.reload()
  // }, [admin])

  return (
    <Router>
      <Switch>
        {admin ? <><Topbar />
          <div className="container">
            <Sidebar />
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/users">
              <UserList />
            </Route>
            <Route path="/user/:userId">
              <User />
            </Route>
            <Route path="/rooms">
              <RoomList />
            </Route>
            <Route path="/entertainments">
              <EntertainmentList />
            </Route>
            <Route path="/product/:productId">
              <Product />
            </Route>
            <Route path="/newservice">
              <NewService />
            </Route>
            <Route path="/orders">
              <OrderList />
            </Route>
          </div></> : <Login />}
      </Switch>
    </Router >
  );
}

export default App;
