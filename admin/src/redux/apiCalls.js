import { publicRequset } from "../requestMethod"
import { loginFailure, loginStart, loginSuccess } from "./userSlice"

export const login = async (dispatch, user) => {
    dispatch(loginStart())
    try {
        const res = await publicRequset.post('/auth/login', user)
        dispatch(loginSuccess(res.data))
    } catch (error) {
        console.log(error)
        dispatch(loginFailure())
    }
}