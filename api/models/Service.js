const mongoose = require("mongoose");

const ServiceSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
            unique: true,
        },
        desc: {
            type: String,
            require: true,
        },
        img: {
            type: String,
            required: true
        },
        categories: {
            type: String,
            required: true
        },
        size: {
            type: String,
        },
        color: {
            type: String,
        },
        price: {
            type: Number,
            required: true
        },
        isBooked: {
            type: Boolean,
            default: false,
        }
    },
    {timestamps: true}
)

module.exports = mongoose.model("Service", ServiceSchema);