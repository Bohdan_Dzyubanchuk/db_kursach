const mongoose = require("mongoose");

const CartSchema = new mongoose.Schema(
    {
        userId: {
            type: String,
            required: true,
            unique: true,
        },
        service:[
            {
                serviceId:{
                    type:String,
                },
                dateFrom:{
                    type:Date,
                    default: new Date()
                },
                dateTo:{
                    type:Date,
                    default: new Date()
                }
            }

        ],
    },
    {timestamps: true}
)

module.exports = mongoose.model("Cart", CartSchema);