const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema(
    {
        userId: {
            type: String,
            unique: false
        },
        service:
        {
            serviceId: {
                type: String,
                unique: false
            },
            dateFrom: {
                type: Date,
                default: new Date(),
                unique: false
            },
            dateTo: {
                type: Date,
                default: new Date(),
                unique: false
            }
        }
        ,
        amount: { type: Number, unique: false },
        status: { type: String, default: "pending" }
    },
    { timestamps: true }
)

module.exports = mongoose.model("Order", OrderSchema);