const express = require("express");
const app = express();
const userRoute = require("./routes/user")
const authRoute = require("./routes/auth")
const serviceRoute = require("./routes/service")
const serviceOrder = require("./routes/order")
const serviceCart = require("./routes/cart")

const mongoose = require("mongoose");
mongoose.connect(
    "mongodb+srv://boh:123@cluster0.tp8t0.mongodb.net/shop?retryWrites=true&w=majority"
).then(() => console.log("DB connected successfully"))
    .catch((e) => console.log(e));

app.use(express.json())
app.use("/api/auth",authRoute )
app.use("/api/service",serviceRoute )
app.use("/api/user",userRoute )


app.use("/api/carts",serviceCart )
app.use("/api/orders",serviceOrder )

app.listen(5000, () =>
    console.log('Backend server started'));