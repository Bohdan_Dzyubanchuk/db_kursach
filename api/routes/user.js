const { verifyToken, verifyTokenAndAuth, verifyTokenAndAdmin } = require("./verifyToken");
const User = require("../models/User")
const CryptoJS = require("crypto-js");
const router = require("express").Router();


//UPDATE
router.put("/:id", verifyTokenAndAuth, async (req, res) => {
    if (req.body.password) {
        req.body.password = CryptoJS.AES
            .encrypt(req.body.password, "admin")
            .toString()
    }
    try {
        const updatedUser = await User.findByIdAndUpdate(req.params.id, {
            $set: req.body,
        }, { new: true })
        res.status(200).json(updatedUser);
    } catch (e) {
        res.status(500).json(e);
    }
})

//DELETE
router.delete("/:id", verifyTokenAndAuth, async (req, res) => {
    try {
        await User.findByIdAndDelete(req.params.id)
        res.status(200).json("Deleted successfully")
    } catch (e) {
        res.status(500).json(e)

    }
})
//GET
router.get("/find/:id", verifyTokenAndAdmin, async (req, res) => {

    try {
        const user = await User.findById(req.params.id)
        const { protectedPass, ...others } = user._doc;
        res.status(200).json(others);
    } catch (e) {
        res.status(500).json(e)

    }
})
router.get("/", verifyTokenAndAdmin, async (req, res) => {
    //const query = req.query.new
    try {
        // const user = query ? await User.find().limit(5).sort({_id: -1}) : await User.find()
        const user = await User.find()
        res.status(200).json(user);
    } catch (e) {
        res.status(500).json(e)

    }
})
router.get("/stats", verifyTokenAndAdmin, async (req, res) => {
    const date = new Date();
    const lastYear = new Date(date.setFullYear(date.getFullYear() - 1))
    try {
        const data = await User.aggregate([
            { $match: { createdAt: { $gte: lastYear } } },
            {
                $project: {
                    month: {
                        $month: "$createdAt",
                    }
                }
            },
            {
                $group: {
                    _id: "$month",
                    total: { $sum: 1 }
                }
            }
        ])
        res.status(200).json(data)
    } catch (e) {
        res.status(500).json(e)
    }
})

router.get("/statsbymonth", verifyTokenAndAdmin, async (req, res) => {
    const date = new Date();
    const lastMonth = new Date(date.setMonth(date.setMonth() - 1))
    try {
        const data = await User.aggregate([
            {
                $match: {
                    "createdAt": {
                        $gte: lastMonth
                    },
                }
            },
            {
                $count: "users"
            }
        ])
        res.status(200).json(data)
    } catch (e) {
        res.status(500).json(e)
    }
})
module.exports = router;