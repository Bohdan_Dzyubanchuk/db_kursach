const User = require("../models/User");
const router = require("express").Router();
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");

//Register
router.post("/register", async (req, res) => {
    const newUser = new User({
        username: req.body.username,
        phone: req.body.phone,
        password: CryptoJS.AES.encrypt(req.body.password, "admin").toString()
    })
    try {
        const user = await newUser.save();
        res.status(201).json(user)
    }
    catch (e) {
        res.status(501).json(e)
    }
})

//LOGIN

router.post("/login", async (req, res) => {
    try {
        const user = await User.findOne({ username: req.body.username });
        if (!user) return res.status(401).json("Wrong credentials");
        const hashedPassword = CryptoJS.AES.decrypt(user.password, "admin");
        const password = hashedPassword.toString(CryptoJS.enc.Utf8);

        if (password !== req.body.password)
            return res.status(401).json("Wrong credentials");

        const accessToken = jwt.sign({
            id: user._id, isAdmin: user.isAdmin,
        }, "admin", { expiresIn: "3d" })

        const { protectedPass, ...others } = user._doc;
        return res.status(200).json({ ...others, accessToken });
    } catch (e) {
        return res.status(500).json("Something went wrong\n" + e);
    }
})
module.exports = router;