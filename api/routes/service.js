const {verifyToken, verifyTokenAndAuth, verifyTokenAndAdmin} = require("./verifyToken");
const Service = require("../models/Service")
const router = require("express").Router();

//CREATE
router.post("/", verifyTokenAndAdmin, async (req, res) => {
    const newService = new Service(req.body)
    try {
        const savedService = await newService.save()
        res.status(200).json(savedService)
    } catch (e) {
        res.status(500).json(e);
    }
})

//UPDATE
router.put("/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        const updatedService = await Service.findByIdAndUpdate(req.params.id, {
            $set: req.body,
        }, {new: true})
        res.status(200).json(updatedService);
    } catch (e) {
        res.status(500).json(e);
    }
})

//DELETE
router.delete("/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        await Service.findByIdAndDelete(req.params.id)
        res.status(200).json("Deleted successfully")
    } catch (e) {
        res.status(500).json(e)

    }
})
//GET
router.get("/find/:id", async (req, res) => {

    try {
        const service = await Service.findById(req.params.id)
        res.status(200).json(service);
    } catch (e) {
        res.status(500).json(e)

    }
})
router.get("/", async (req, res) => {
    const qNew = req.query.new
    const qCategory = req.query.category
    try {
        let service;
        if(qNew){
            service = await  Service.find().sort({createdAt:-1}).limit(5)
        }else if(qCategory){
            service = await Service.find({categories: qCategory})
        }
        else{
            service = await Service.find();
        }
        res.status(200).json(service);
    } catch (e) {
        res.status(500).json(e)

    }
})
module.exports = router;