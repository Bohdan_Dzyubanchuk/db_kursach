const { verifyToken, verifyTokenAndAuth, verifyTokenAndAdmin } = require("./verifyToken");
const Order = require("../models/Order")
const User = require("../models/User");
const router = require("express").Router();

//CREATE
router.post("/", async (req, res) => {
    const order = new Order(req.body)
    try {
        const savedOrder = await order.save()
        res.status(200).json(savedOrder)

    } catch (e) {
        res.status(500).json(e.message);
    }
})

//UPDATE
router.put("/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        const updatedOrder = await Order.findByIdAndUpdate(req.params.id, {
            $set: req.body,
        }, { new: true })
        res.status(200).json(updatedOrder);
    } catch (e) {
        res.status(500).json(e);
    }
})

//DELETE
router.delete("/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        await Order.findByIdAndDelete(req.params.id)
        res.status(200).json("Deleted successfully")
    } catch (e) {
        res.status(500).json(e)

    }
})
//GET
router.get("/find/:userId", verifyTokenAndAuth, async (req, res) => {

    try {
        const order = await Order.find({ userId: req.params.userId })
        res.status(200).json(order);
    } catch (e) {
        res.status(500).json(e)

    }
})
router.get("/", verifyTokenAndAdmin, async (req, res) => {

    try {
        const orders = await Order.find()
        res.status(200).json(orders);
    } catch (e) {
        res.status(500).json(e)

    }
})

//MONTHLY INCOME

router.get("/incomebymonth", verifyTokenAndAdmin, async (req, res) => {
    const date = new Date();
    const lastMonth = new Date(date.setMonth(date.setMonth() - 1))
    const previousMonth = new Date(new Date().setMonth(lastMonth.setMonth() - 1))
    try {
        const data = await Order.aggregate([
            {
                $match: {
                    "service.dateFrom": {
                        $gte: previousMonth
                    },
                    status: "agreed"
                }
            },
            {
                $project: {
                    month: {
                        $month: "$service.dateFrom",
                    },
                    sales: "$amount"
                }
            },
            {
                $group: {
                    _id: "$month",
                    total: { $sum: "$sales" }
                }
            }
        ])
        res.status(200).json(data)
    } catch (e) {
        res.status(500).json(e)
    }
})
router.get("/incomecount", verifyTokenAndAdmin, async (req, res) => {
    const date = new Date();
    const lastMonth = new Date(date.setMonth(date.setMonth() - 1))
    const previousMonth = new Date(new Date().setMonth(lastMonth.setMonth() - 1))
    try {
        const data = await Order.aggregate([
            {
                $match: {
                    "service.dateFrom": {
                        $gte: previousMonth
                    },
                    status: "agreed"
                }
            },
            {
                $count: "count"
            }
        ])
        res.status(200).json(data)
    } catch (e) {
        res.status(500).json(e)
    }
})
router.get("/stats", verifyTokenAndAdmin, async (req, res) => {
    const date = new Date();
    const lastYear = new Date(date.setFullYear(date.getFullYear() - 1))
    try {
        const data = await Order.aggregate([
            { $match: { "service.dateFrom": { $gte: lastYear } } },
            {
                $project: {
                    month: {
                        $month: "$service.dateFrom",
                    }
                }
            },
            {
                $group: {
                    _id: "$month",
                    total: { $sum: 1 }
                }
            }
        ])
        res.status(200).json(data)
    } catch (e) {
        res.status(500).json(e)
    }
})
module.exports = router;